# Changelog

## 1.0.2 (unreleased)

- No changes yet.

## 1.0.1 (2028-11-29)

- Drop support for Python 2.6-3.6, which have reached end-of-life.

- Add support for Python 3.9-3.11.

- Modernize Python packaging to [configure Setuptools using pyproject.toml](https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html).

## 1.0.0 (2020-02-10)

- This release fixes some minor lint and packaging issues, and updates the
  major version number to reflect the package's stable status.

- Add testing for Python 3.7 and 3.8.

## 0.0.1 (2018-08-06)

- Initial release.
